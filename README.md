
# Formulario di Elementi di Topologia Algebrica

Versione più recente del PDF: <https://gitlab.com/aziis98-notes/formulari/eta/-/raw/main/main.pdf>

Generated from template: <https://github.com/aziis98/template-latex>

## Structure

All source files should be placed in [`src/`](./src), they are copied to the [`.cache`](./.gitignore) directory, rendered and then [`main.pdf`](./main.pdf) is copied back into the root of the project.

## Usage

To be organized I've recently started using this [Makefile](./Makefile) to manage LaTeX projects, so just use `make`, `make all` or `make main.pdf` to build the project and generate the output PDF.

The alternative is to run [`./watch`](./watch) that uses `entr` to watch and rerender the PDF when the source files change (and shows a notification with `notify-send`).

### Live reload with entr (just `./watch` now)

If you have `entr` installed you can also have "live reload" on save with

```bash
find src/ -type f | entr make
```

that will automatically rebuild [`main.pdf`](main.pdf).
